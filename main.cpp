/*
 * Copyright (C) 2009 Josh A. Beam
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *   1. Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <cstdio>
#include <cmath>
#include <SDL.h>
#include "Rasterizer.h"

#define WINDOW_WIDTH 1280
#define WINDOW_HEIGHT 1024
#define M_PI 3.14159
const float size = 110.0f;

static bool g_Running = true;

static void
HandleKeyEvent(const SDL_Event &event)
{
	switch(event.key.keysym.sym) {
		default:
			break;
		case SDLK_ESCAPE:
			g_Running = false;
			break;
	}
}

static void
HandleEvent(const SDL_Event &event)
{
	switch(event.type) {
		default:
			break;
		case SDL_QUIT:
			g_Running = false;
			break;
		case SDL_KEYDOWN:
			HandleKeyEvent(event);
			break;
	}
}

int
main(int argc, char *argv[])
{
	// initialize SDL
	if(SDL_Init(SDL_INIT_VIDEO) != 0) {
		fprintf(stderr, "SDL_Init failed\n");
		return 1;
	}

	// create window for drawing
	SDL_Surface *screen = SDL_SetVideoMode(WINDOW_WIDTH, WINDOW_HEIGHT, 32, SDL_HWSURFACE);
	if(!screen) {
		SDL_Quit();
		fprintf(stderr, "SDL_SetVideoMode failed\n");
		return 1;
	}

	SDL_WM_SetCaption("Triangle Rasterization Demo - http://www.3ddrome.com/", NULL);

	Rasterizer rast;
	rast.SetFrameBuffer((uint32_t *)screen->pixels, WINDOW_WIDTH, WINDOW_HEIGHT);

	unsigned int lastTicks = SDL_GetTicks();

	float r = 0.0f;

	float length = 50;

	float g_x1 = (WINDOW_WIDTH / 2);
	float g_y1 = (WINDOW_HEIGHT / 2);
	float g_x2, g_y2;

	// loop until we're done running the program
	while(g_Running) {
		Uint8* keystate = SDL_GetKeyState(NULL);

		//continuous-response keys
		if (keystate[SDLK_LEFT])
		{
			r -= 0.005f;
		}
		else if (keystate[SDLK_RIGHT])
		{
			r += 0.005f;
		}
		else if (keystate[SDLK_UP])
		{
			g_x1 += cosf(r + M_PI / 2.0);
			g_y1 += sinf(r + M_PI / 2.0);
		}

		g_x2 = g_x1 + cosf(r + M_PI / 2.0) * length;
		g_y2 = g_y1 + sinf(r + M_PI / 2.0) * length;

		// handle events
		SDL_Event event;
		while(SDL_PollEvent(&event))
			HandleEvent(event);

		// lock surface and clear framebuffer
		SDL_LockSurface(screen);
		rast.Clear();

		// calculate coordinates for triangle

		// colors for each point of the triangle
		Color color1(1.0f, 0.0f, 0.0f);
		Color color2(0.0f, 1.0f, 0.0f);
		Color color3(0.0f, 0.0f, 1.0f);

		// render triangle
		rast.DrawLine(color1, g_x1, g_y1, color1, g_x2, g_y2);

		// unlock and update surface
		SDL_UnlockSurface(screen);
		SDL_UpdateRect(screen, 0, 0, 0, 0);

		// calculate the number of seconds that
		// have passed since the last update
		unsigned int ticks = SDL_GetTicks();
		unsigned int ticksDiff = ticks - lastTicks;
		if(ticksDiff == 0)
			continue;
		float time = ticksDiff / 1000.0f;
		lastTicks = ticks;

		// update rotation
		//r += M_PI / 2.0f * time;

		// display frames per second
		unsigned int fps = 1000 / ticksDiff;
		printf("Frames per second: %u    \r", fps);
	}

	SDL_Quit();
	return 0;
}
